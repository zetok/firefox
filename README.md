# Firefox Tuning Guide

## Intro

A brief opinionated guide for people who are new to Firefox detailing several important configuration options for Firefox which can improve privacy, security, and productivity.
Some of these options are easy to get wrong or forget when you're starting out with Firefox, and it's critical to minimize the information you leak to unwanted third parties.

This guide is aimed at non-technical beginners. Feel free to adopt as many or as few of these suggestions as you would like. It is a work in progress and some aspects may need corrections. The security threat model only takes third party advertisers into account; no Tor or VPN use mentioned.

If you are new to firefox or switching to it, there are also notes available at the r/Firefox wiki: https://www.reddit.com/r/firefox/wiki/index. See https://www.reddit.com/r/firefox/wiki/switching-to-firefox for "switching to Firefox" concerns. It has become clear that we Internet users must support Firefox long-term to be given the choice of an ad-free web, especially on sites which don't allow payment to avoid ads. This is due to changes to Chromium discussed [here](https://www.reddit.com/r/firefox/comments/buifjk/chrome_to_limit_full_ad_blocking_extensions_to/) and [here](https://www.reddit.com/r/firefox/comments/busxxt/creator_of_ublock_origins_poignant_summary_on/). The Firefox team has proven that even if they make mistakes in e.g. deployment, they will [respond honestly and quickly](https://www.reddit.com/r/firefox/comments/bmphdp/what_an_outstanding_response_from_mozilla_honestly/).

(This guide is still being revised, corrected, and improved. Need to link to more classic r/Firefox posts such as [this](https://www.reddit.com/r/firefox/comments/anuou8/why_do_i_need_a_4ghz_quadcore_to_run_facebook/). Need to talk about [WebRender](https://www.reddit.com/r/firefox/comments/bqg49u/after_3_years_and_8_months_of_work_webrender_will/) as it's being rolled out now, performance benefits users see in Firefox like [here](https://www.reddit.com/r/firefox/comments/afpxf4/firefox_with_webrender_vs_chrome_scrolling/). Remember to acknowledge common complaints and areas where Firefox can improve so people don't think the analysis is unrealistic.)

## 0. Setup

Before tightening privacy features of Firefox, you probably want to make yourself harder to fingerprint in the future. You can do this by clearing all existing website data and cookies from Firefox.
> (Cookies are small information records which websites store in your browser as you browse, and may be used for tracking or non-tracking purposes. Site data includes data such as images which won't need to be reloaded again if you visit the pages they originated from again. However, they indirectly tell the server that you visited those pages before.)

First, make sure you would be able to get back into all accounts - after clearing, you will need to re-login to every service. The passwords you saved in Firefox will remain, since you only deleted website data and cookies, not saved passwords. However, if you are logged into a service, but didn't write down your password to that service and didn't save it in Firefox, then when you clear all website data, you'll be logged out and won't know the password to get back in. Before clearing data, you should reset your passwords and have Firefox save them when you log in again, for every service which you didn't know the password to. If you use and trust a password manager, that might have all your passwords.

Clear all existing website data from Firefox, especially cookies, by checking both checkboxes in the `Clear Data...` menu. (Preferences... > Privacy & Security > Clear Data... > both checkboxes checked > Clear.)

For maximum privacy, you should finish all configuration in this guide before going back to your normal web browsing.

## 1. Extensions

### 1.1. Extension: uBlock Origin

> Finally, an efficient blocker. Easy on CPU and memory.

The one tracker blocker to rule them all. Nothing more to say here if you only care about the easy, default "fire and forget" mode - just install and you are set. Get it at https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/. Further reasons for uBlock Origin over alternatives [here](https://www.reddit.com/r/firefox/comments/bvoxeo/use_ublock_origin_not_adblock_this_guy_is_right/).

### 1.2. Extension: Multi-Account Containers

> Firefox Multi-Account Containers lets you keep parts of your online life separated into color-coded tabs that preserve your privacy. Cookies are separated by container, allowing you to use the web with multiple identities or accounts simultaneously.

Made by Mozilla themselves, this extension is trustworthy and should be the first one you install. Go to https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/ to get it. Multi-Account Containers forms the backbone of Firefox's advantages over all other browsers, because all private browsing windows are all part of the same "private browsing session". In most browsers, there can only be one private browsing session at a time. All of your private windows "know" what the other private windows are doing. The data from the private browsing session is only deleted when the last private window is exited. With Multi-Account Containers, you're able to have as many different browsing sessions as you want simultaneously, and none of them know about each other.

Your mental model of browsing profiles becomes:

* The default profile when you are just making a new tab or new window. No special colors show for these tabs.

* In addition, one new profile for each different container you have.

You start out with four suggested containers, and you should customize these further so that choosing the right container becomes quick and easy. To do this, click and hold the `+` (New Tab) icon in the top bar of tabs. Then drag the cursor down to select `Manage Containers`. You can customize the color and name of each container. Names which are "kebab-style chains" of associations or interests e.g. `firefox-rust-servo` might be a good naming strategy for you.

To open a new tab in that container, you use the same interface: click and hold the `+` button, and drag the cursor to the type of container you want.

Think carefully about how you want your online identities to be divided among the containers, and remember that for maximum effectiveness, you should "divide up" your accounts:

* One logged-in account per container type: suppose you have two different Reddit accounts. Then you log into and use Reddit with Account A in Container A, and log into and use Reddit with Account B in Container B. You would then never log out of Account A and log into Account B all within Container A. Account B would only be used within Container B.

* Even better: only certain sites (domains) per container type. Suppose you had a Reddit account and a Facebook account, and you didn't want them knowing about each other. Then you would log into and use Reddit in Container A, and never visit Facebook in Container A; and log in and use Facebook in Container B, and never visit Reddit in Container B. This provides maximum effectiveness against fingerprinting and correlation of browsing habits, but is more restrictive.

* Also, the "one-time pad" model for websites you don't trust: create a new container, and open a new tab in that container. Perform the browsing, and then when done, go to Manage Containers again and Remove that container. All of its data will be deleted forever. This is made simple by the [`Temporary Containers`](#13-extension-temporary-containers) extension. Of course, the standard Private Browsing window feature of Firefox can be used for this "one-time pad" purpose if you aren't using it for anything else.

There is a small learning curve to build the habit of dragging to open a new container tab if you are constantly opening and closing tabs, but it becomes fast and natural over time. It's simplest when your computer can support having many tabs open, since those containers are already open; and remember that any new tabs generated from clicking links in an existing container will share that same container.

> (Extremely rare concern: one small corner case around the private browsing window to watch out for, if you use the keyboard combination shortcut to open it. I have never run into this problem, but just so you are aware:
> * Suppose you have just browsed to a site you don't trust or that features extensive fingerprinting or trackers, and which you have used the private browsing window for. You close it and carry on with your day.
> * However, later on you try to open a new private browsing window using the hotkey Ctrl+Shift+P. If you're used to Chrome or Brave, you might accidentally use Ctrl+Shift+N, which is the "Undo Close Window" hotkey in Firefox ([keyboard shortcuts][kb_shortcuts]).
> * The untrusted site is then reopened in a new window with your default profile - not ideal for cookies. You can change default keyboard shortcuts using the "Saka Key" extension, but this requires trusting the extension authors.)

### 1.3. Extension: Temporary Containers

> Open tabs, websites, and links in automatically managed disposable containers. Containers isolate data websites store (cookies, storage, and more) from each other, enhancing your privacy and security while you browse.

Temporary Containers greatly simplify the previously tedious workflow with the Multi-Account Containers for the "one-time pad" case. Get it at https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/.

Instead of manually creating and deleting containers, this addon will automate creation and deletion of temporary containers with just a simple click on the button added by the extension to the right of the `+` (New Tab) icon in the top bar of tabs.

For more advanced usage, the extension can be configured to automatically open temporary containers for new tabs that aren't already in a container, or just use it for specified blacklist / whitelist of websites it should automatically be used with.

This extension has plenty of other configurable options for advanced usage, allowing the user to precisely specify how it should work, tending to all possible use cases.

### 1.4. Extension: Auto Tab Discard

> Auto Tab Discard a lightweight extension that uses the native method (tabs.discard) to unload or discard browser tabs which significantly reduces the memory footprint of your browser when many tabs are in use.
>
> If you are used to keeping many tabs opened at once during your work, this extension could help you to keep your browser fresh with low memory usage by parking (hibernating) inactive tabs. This also helps portable devices to save battery.

If you have lots of tabs and little of RAM, this addon will help. It provides the tab unloading feature that is available in Chrome/Chromium that wasn't implemented in Firefox due to concerns of disrupting users' work.

Get it at https://addons.mozilla.org/en-US/firefox/addon/auto-tab-discard/ .

### 1.5. Enable Extensions in Private Windows

After installing all desired extensions, make sure to go into Addons (about:addons), click `Extensions` at the left, and for each of the extensions you have installed (assuming you trust them all), click on their respective panel and change the `Run in Private Windows` radio button to `Allow`.

## 2. Changes Through Preferences (about:preferences)

### 2.1. General

* Checking `Restore previous session` helps to keep you sane if you have a large number of different containers and you run into some rare corner case where Firefox becomes slow and you must restart. You can restart in peace knowing all your containers and tabs will be restored.

* Checking `Warn you when quitting the browser` helps to avoid accidental exits.

* You might consider not setting the current Firefox browser you are in as your default browser. Consider the case where a desktop application or service tries to open a link. It will open in the default browser you have told your OS to use. However, this might be a service that you didn't know was going to connect to the internet, or a service you only wanted to connect to through a container you had arranged just for that service. In that case, you would want your default browser to convey the minimum possible information to whatever site it opens - it should be a perpetually blank slate that forgets everything when closed. So you would want to select a secondary browser.
    * A good choice would be a different version of Firefox. You are most likely using the Stable version of Firefox, with an orange fox on its icon. You could choose a version like the Beta version for your secondary browser. Get it from https://www.mozilla.org/en-US/firefox/channel/desktop/. Once downloaded, you can configure it to fully delete all of the site data or cookies it acquired by checking the `Delete cookies and site data when Firefox is closed` checkbox.
    * Installing uBlock Origin and changing settings as recommended in 2. and 3. in this secondary, "default" browser further defends against fingerprinting. Be sure to set the secondary browser as your OS' default browser before quitting.

* Further down the page, you can find the `Applications` table where you can customize how Firefox handles the files you download from the web or the applications you use while browsing. It is worthwhile to set the `pdf` file type, if shown, to `Save File` instead of trying to open it in the browser or ask. PDFs viewed in the browser are a security risk, and it's preferable to use a standalone PDF reader like Adobe Reader to view any PDFs you come across. (Ideally, you will have disabled JavaScript execution and access to the internet in your PDF reader if you don't require that functionality.)

* At the very bottom of the page are `Network Settings`. Click on `Settings…` to
  show `Connection Settings` pop-up. At the bottom of the pop-up check `Enable
  DNS over HTTPS` and click `OK` to apply the setting.
    * DNS over HTTPS (DoH) encrypts DNS requests preventing third parties from
      spying on and possibly hijacking DNS requests. E.g. it will be harder for
      your ISP to know the names of the websites you are interested in, and some
      ISPs aren't happy about it: https://old.reddit.com/r/privacy/comments/c92ywt/uk_internet_service_provider_association_calls/
        * **DNS resolver still knows which websites you are interested in, and
          it is possible to figure out possible website you're connecting to
          based on the IP address of the website.** Encrypted DNS is still an
          improvement over an unencrypted requests.
        * https://blog.nightly.mozilla.org/2018/06/01/improving-dns-privacy-in-firefox/

### 2.2. Home

* Selecting `Blank Page` for both `Homepage and new windows` and `New tabs`, and unchecking all boxes in Firefox Home Content, lets you stay focused and cuts down on the number of network requests your browser makes. However, you should choose what is most convenient for your use case.

### 2.3. Search

* Selecting DuckDuckGo as the default search engine preserves privacy. If you ever need to fall back to Google search, `!g` followed by a space and then your search term performs a Google search of that term.

* Unchecking all of the `Provide search suggestions` checkboxes helps to avoid password leakage if you copy a password and accidentally paste it into the address bar, which could send it off to a search engine provider.

### 2.4. Privacy & Security

* Among the `Content Blocking` option panels, choose `Custom` (**not just** `Strict`, which isn't strict enough on its own).

* Check `Trackers`, select `In all windows`.
* Check `Cookies`, select `All third-party cookies`.
* Check `Cryptominers`.
* Check `Fingerprinters`.

* For `Logins and Passwords`, I personally trust Firefox with all of my logins and passwords for websites and check `Ask to save ...`, but you may be using an external password manager program. In general it is good to trust as few organizations as possible, but the choice is yours. Passwords 60 or more characters long could be a nice convention.

* For `History`, selecting `Remember history` could be valuable in the future. More on that later.

* For `Permissions` you could choose to set all three besides Notifications to `Block new requests ...` for safety. For example, for people you trust enough to video chat with, you could use Signal if you know them in real life well enough to exchange phone numbers; and there is Skype. For Google Hangouts you can use the Brave browser with the Hangouts functionality enabled, or a separate device.

* Ensure `Warn you when websites try to install add-ons` is checked.

* Ensure `Prevent accessibility services from accessing your browser` is checked. This helps prevent fingerprinting.

* If you are paranoid or extra security-conscious you can uncheck all boxes in `Data Collection and Use`.

* Ensure all `Block dangerous and deceptive content` checkboxes are checked, and leave the settings for `Certificates` at their defaults.

### 2.5. Sync

* Up to you.

## 3. Changes Through about:config

1. Click through, accept that you will be careful here.
2. Use the search bar and type the setting key name (or some fraction of it, it should come up quickly). Change this value from default to the one listed here by double-clicking or right-clicking.

### `network.security.esni.enabled`

Set to: `true`

If the website supports encrypted SNI, this will help prevent revealing the name of the website that you're visiting.

More info:

- https://blog.mozilla.org/security/2018/10/18/encrypted-sni-comes-to-firefox-nightly/
- https://blog.cloudflare.com/encrypt-that-sni-firefox-edition/

### `privacy.resistFingerprinting`

Set to: `true`

This is important for more advanced fingerprinting protection like letterboxing and window size.

## 4. Preventing Countermeasures

### 4.1. URL Tracking

Certain websites, such as Medium and many news websites, attempt to get around containers and other measures to create separate profiles by using various "URL tracking" techniques. This usually manifests as a unique sequence of characters appended to whatever URL you're visiting, e.g. the `-7d8950fbb5d4` in `https://medium.com/@addyosmani/the-cost-of-javascript-in-2018-7d8950fbb5d4`. This is why the Safari browser's Intelligent Tracking Protection will always remain handicapped - if e.g. 5 or fewer browsers visit that unique link, it's a good chance those sessions and their cookies are correlated. A mitigation to this is, for news articles, to use a search engine to search for the title of the article you want to read, instead of clicking on the link directly from e.g. a social media feed it appeared in. Then there is a higher chance you will be using a tracking suffix used by many people.

## 5. Bonus Customization

Thanks to how customizable Firefox is, there are further improvements we can make beyond the default settings if desired.

### 5.1. Fit More Tabs Before Overflowing

Coming from Chrome or Brave, you might be used to tabs piling up with no horizontal scrolling instead of reaching a minimum width and then overflowing into a horizontal scroll tab bar. To fit more tabs at the top of a window before the horizontal scrolling overflow is triggered, within about:config, set `browser.tabs.tabMinWidth` to some smaller integer like `10`. This is one way of seeing more tabs at once without installing a third-party extension. Some people use the [Tree Style Tab](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/) extension.

### 5.2. Unlimited History

Within about:config, if you right-click and create the preference `places.history.expiration.max_pages` with type `integer` and value set to the largest 32-bit signed integer (`2147483647`), then Firefox won't delete any pages from your history until 2147483647 pages have been stored (which is nearly limitless for a normal lifetime of browsing). This could be valuable when trying to refer back to something, because you can go directly back to the page instead of through a search engine which may or may not resurface the past result. Over the years, your own tastes and references are curated for you to look back on. As you type in the new tab bar, your entire history of old pages with matching fragments in the title and/or url is available to you. You can lower this number in the future if it's too taxing on your computer's memory; I have encountered no issues after a month so far.

You can then set `places.history.expiration.interval_seconds` to a higher number like `2592000` for performance - that is the number of seconds between times when the browser checks if it can evict entries from its history.

> (Solution from https://superuser.com/questions/895302/how-do-i-set-max-browsing-history-size/995459#995459.)

> (If you encounter some abusive website which pushes many pages or redirects into your history you can always clear history from e.g. the last hour or last day, or use a private browsing window for that site since pages from private browsing don't go into your history.)

## 6. Daily Use

You likely already know about these but just putting for absolute beginners. Tips which can make your daily use and browsing faster. Mac users substitute `Cmd` for `Ctrl` wherever `Ctrl` is shown.

* `Ctrl+T` new tab.
* `Ctrl+W` close tab.
* `Ctrl+N` new window.
* `Ctrl+F` and start typing to find text in page.
* `Ctrl+P` to print.

Other [keyboard shortcuts][kb_shortcuts].


## Recommendations

There are other places that can be recommended wrt. tuning the browser for user needs. Here's an incomplete list of resources:

- https://www.privacytools.io/browsers/
- https://www.reddit.com/r/firefox/wiki/


## Text under CC0

This text is licensed under Creative Commons Zero (CC0), for details see [`LICENSE`].


[`LICENSE`]: /LICENSE
[kb_shortcuts]: https://support.mozilla.org/en-US/kb/keyboard-shortcuts-perform-firefox-tasks-quickly
